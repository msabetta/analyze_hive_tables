# Utilizzare un'immagine base di Hadoop per supportare Hive e Impala
#FROM apache/hadoop:2.10.2
FROM centos:centos7.9.2009

USER root

RUN cd /etc/yum.repos.d/
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

# Aggiornamento e installazione dei prerequisiti utilizzando yum
RUN yum update -y && \
    yum install -y \
    java-1.8.0-openjdk \
    python \
    curl \
    wget \
    && yum clean all

# Impostare variabili d'ambiente per Java e Hadoop
ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV HADOOP_HOME=/usr/local/hadoop
ENV PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$JAVA_HOME/bin

# Scarichiamo ed installiamo Apache Hive on Tez
RUN wget https://downloads.apache.org/hive/hive-3.1.2/apache-hive-3.1.2-bin.tar.gz && \
    tar -xzf apache-hive-3.1.2-bin.tar.gz && \
    rm apache-hive-3.1.2-bin.tar.gz && \
    mv apache-hive-3.1.2-bin /opt/hive

# Configurare Hive per utilizzare Tez
RUN echo "set hive.execution.engine=tez;" >> $HADOOP_HOME/hive/conf/hive-site.xml

# Scarichiamo ed installiamo Impala
RUN wget https://downloads.apache.org/impala/impala-4.0.0/impala-4.0.0.tar.gz && \
    tar -xzf impala-4.0.0.tar.gz && \
    rm impala-4.0.0.tar.gz && \
    mv impala-4.0.0 /opt/impala

# Installare Apache Hue
RUN git clone https://github.com/cloudera/hue.git \
    && cd hue \
    && make apps

# Esporre le porte necessarie
EXPOSE 8888 10000 21050

# Avviare i servizi
CMD service hue start && $HADOOP_HOME/hive/bin/hiveserver2 && /usr/local/impala/bin/impalad