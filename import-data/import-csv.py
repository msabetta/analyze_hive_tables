from pyhive import hive
import csv
import docker
import os

def copy_csv_to_docker_container(container_name, local_csv_path, container_csv_path):
    # Connetti al client Docker
    client = docker.from_env()

    try:
        # Trova il container specificato
        container = client.containers.get(container_name)

        # Verifica se il file CSV locale esiste
        if not os.path.exists(local_csv_path):
            print("Il file CSV locale non esiste.")
            return

        # Verifica se il container esiste e sta eseguendo
        if container:
            # Copia il file CSV nella directory del container
            container.put_archive('/', (local_csv_path, container_csv_path))

            print("Il file CSV è stato copiato con successo nel container.")
        else:
            print("Il container specificato non esiste o non è in esecuzione.")
    except docker.errors.NotFound:
        print("Il container specificato non è stato trovato.")
    except docker.errors.APIError as e:
        print(f"Si è verificato un errore API Docker: {str(e)}")


if __name__ == '__main__':
    #csv_file_path_local = '/home/ubuntu/analyze_hive_tables/impiegati.csv'
    csv_file_path_container = '/opt/impiegati.csv'

    #copy_csv_to_docker_container("docker-hive_hive-server_1", csv_file_path_local, csv_file_path_container)

    # Connessione a Hive
    conn = hive.Connection(host='0.0.0.0', port=10000, username='hive')

    # Nome della tabella Hive
    hive_table_name = 'company.impiegati'

    # Query per caricare i dati
    query = f"LOAD DATA LOCAL INPATH '{csv_file_path_container}' INTO TABLE {hive_table_name}"

    try:
        # Esegui la query
        cursor = conn.cursor()
        cursor.execute(query)
        print("File CSV importato con successo nella tabella Hive.")
    except Exception as e:
        print(f"Si è verificato un errore durante l'importazione del file CSV: {str(e)}")
    finally:
        # Chiudi la connessione
        conn.close()