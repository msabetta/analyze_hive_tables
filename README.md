# analyze_hive_tables


## Getting started

This is a project written in python3 for parsing hive tables and storing parsing queries in a MongoDB database

## Installation

### Python 3 installation steps
https://www.liquidweb.com/kb/how-to-install-python-3-on-centos-7/

### pip installation steps
https://www.liquidweb.com/kb/how-to-install-pip-on-centos-7/

### Docker installation steps 
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-centos-7

### Hive installation steps
$ cd docker-hive <br>
$ docker-compose up -d

### Mongo DB installation steps
$ cd scripts <br>
$ ./create_mongo_container.sh

## Usage
$ pip install -r requirements.txt <br>
$ python3 main.py

