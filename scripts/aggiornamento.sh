#!/bin/bash

# Imposta la data corrente
giorno_corrente=$(date +"%Y-%m-%d") 
# Crea la directory e il file di log
mkdir -p /storage/analyze_table/analyze_$giorno_corrente
touch /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
# Ciclo per ogni database
for database in $(hive -e "SHOW DATABASES"); do
 # Stampa il nome del database
 echo "Analisi del database: $database" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
 echo "-----------------------------" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log 
 # Ciclo per ogni tabella nel database
 for table in $(hive -e "SHOW TABLES IN $database"); do
	# Stampa il nome della tabella
  echo "Analisi della tabella: $database.$table" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
  echo "-----------------------------" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
  # Controlla se la tabella è partizionata
  partitioned=$(hive -e "SHOW PARTITIONS $database.$table")
  # Se la tabella è partizionata
  if [[ $partitioned =~ "org.apache.hadoop.hive.ql.io.BucketedHiveInputFormat" ]]; then
		echo "$database.$table è una tabella partizionata" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
		# Ciclo per ogni partizione della tabella
		for partition in $(hive -e "SHOW PARTITIONS $database.$table"); do
			# Stampa il nome della partizione
			echo "Analisi della partizione: $database.$table.$partition" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
      echo "-----------------------------" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
		      	# Esegui la query per ottenere l'elenco delle sottopartizioni
			subpartitions=$(hive -e "SHOW PARTITIONS $database.$table;" | awk -F '=' '{print $2}')

			# Verifica se sono presenti sottopartizioni
			if [ -z "$subpartitions" ]; then
		    		echo "La tabella $database.$table non ha sottopartizioni."
		    		# Esegui analyze table sulla partizione
			hive -e "ANALYZE TABLE $database.$table PARTITION ($partition) COMPUTE STATISTICS FOR COLUMNS;" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
			else
			    	# Loop attraverso le sottopartizioni e analizza la tabella
			    	while IFS= read -r subpartition; do
					echo "Analizzando la sottopartizione: $subpartition"
					hive -e "ANALYZE TABLE $database.$table PARTITION ($partition, $subpartition) COMPUTE STATISTICS FOR COLUMNS;" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
					echo "Analisi completata per la sottopartizione: $subpartition"
			    	done <<< "$subpartitions"
			fi
      
			# Controlla il codice di ritorno
			if [[ $? -ne 0 ]]; then
				echo "Errore durante l'analisi di $database.$table" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
      else
        echo "Analisi di $database.$table svolta correttamente" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
			fi
		done
  # Se la tabella non è partizionata
  else
		echo "$database.$table è una tabella non partizionata" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
		# Esegui analyze table sulla tabella
		hive -e "ANALYZE TABLE $database.$table COMPUTE STATISTICS FOR COLUMNS;" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
		if [[ $? -ne 0 ]]; then
			echo "Errore durante l'analisi di $database.$table" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
    else
      echo "Analisi di $database.$table svolta correttamente" >> /storage/analyze_table/analyze_$giorno_corrente/$giorno_corrente.log
		fi
  fi
  done
done


#Implementazione visualizzazione timestamp ultima ANALYZE TABLE eseguita

last_hive_query=$(history | grep 'hive' | tail -n 1 | cut -d' ' -f4-)
table_name=$(echo $last_hive_query | awk '{print $3}')
#table_name=$(echo $last_hive_query | sed 's/.*analyze table \([^ ]*\).*/\1/')
analyze_timestamp=$(hive -e "DESCRIBE FORMATTED $table_name" | grep 'Last Analyzed' | awk '{print $3, $4}')
#analyze_timestamp=$(hive -e "DESCRIBE FORMATTED $table_name" | grep 'Last Analyzed' | awk '{print $3, $4}')
echo "Timestamp of last ANALYZE TABLE for $table_name: $analyze_timestamp"


#Implementazione algoritmo di controllo delle risorse su YARN durante l'esecuzione di una data applicazione (job) 

application_id=$(yarn application -list -appStates RUNNING)
#application_id=$(yarn application -list | grep "nome_dell_applicazione" | sort -k4,4nr | head -n 1 | awk '{print $1}')
info_job_executed=$(yarn application -status $application_id)
used_resourses=$(yarn application -status $application_id | grep 'Used Resources:')
used_memory=$(echo "$application_info" | grep 'Used Containers' | awk '{print $3}')
used_cpu=$(echo "$application_info" | grep 'Used vCores' | awk '{print $3}')

queue_name=default #default,development,production
queue_info=$(yarn queue -status $queue_name)
total_memory=$(echo "$queue_info" | grep 'Current Capacity' | awk '{print $4}')
total_cpu=$(echo "$queue_info" | grep 'Current Capacity' | awk '{print $6}')


# Calcolo della percentuale di utilizzo di memoria
used_memory_percent=$(echo "scale=2; ($used_memory / $total_memory) * 100" | bc)

# Calcolo della percentuale di utilizzo di CPU
used_cpu_percent=$(echo "scale=2; ($used_cpu / $total_cpu) * 100" | bc)



#Threshold definition of memory and cpu
memory_threshold=90
cpu_threshold=90

#Gestione dell'allocazione delle risorse di memoria RAM
if (( $(echo "$used_memory_percent > $memory_threshold" | bc -l) )); then
    echo "Memoria utilizzata sopra la soglia, sono necessarie risorse aggiuntive."
    # Aggiungi codice per allocare risorse aggiuntive di memoria
    # Esempio: yarn application -request-resource <application_id> -memory <additional_memory>
    additional_memory=2GB #500MB,1GB,2GB,4GB,8GB
    yarn application -request-resource $application_id -memory $additional_memory
fi

#Gestione dell'allocazione delle risorse relative alla CPU
if (( $(echo "$used_cpu_percent > $cpu_threshold" | bc -l) )); then
    echo "CPU utilizzata sopra la soglia, sono necessarie risorse aggiuntive."
    # Aggiungi codice per allocare risorse aggiuntive di CPU
    # Esempio: yarn application -request-resource <application_id> -vcores <additional_cpu>
    additional_cpu=2 #1,2,4,8
    yarn application -request-resource $application_id -vcores $additional_cpu
fi








  



