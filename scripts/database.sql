CREATE DATABASE company;

USE company;

CREATE TABLE IF NOT EXISTS impiegati (
    id INT,
    nome VARCHAR(50),
    cognome VARCHAR(50),
    dipartimento VARCHAR(50),
    stipendio FLOAT,
    data_assunzione VARCHAR(50)
);