from pyhive import hive
from pymongo import MongoClient
from datetime import datetime
from solr import Solr


def execute_hive_analyze_query(table_name, partition):
    query = f"ANALYZE TABLE {table_name} PARTITION ({partition}) COMPUTE STATISTICS FOR COLUMNS"
    hive_cursor.execute(query)
    return query


def save_query(query, timestamp):
    mongo_collection.insert_one({'query': query, 'timestamp': timestamp})


def get_last_query_timestamp():
    last_query = mongo_collection.find_one(sort=[('timestamp', -1)])
    if last_query:
        return last_query['timestamp']
    return None


def connect_solr(host, table):
    solr_conn = Solr('http://' + host + ':8983/solr/' + table)
    return solr_conn


def save_query_into_solr(query, timestamp):
    solr_conn = connect_solr('localhost', 'hive_queries')
    doc = {
        'query': query,
        'timestamp': timestamp.isoformat()
    }
    solr_conn.add([doc])
    solr_conn.commit()


def get_last_query_timestamp_solr():
    solr_conn = connect_solr('localhost', 'hive_queries')
    response = solr_conn.query('*:*', sort='timestamp desc', rows=1)
    if len(response.results) > 0:
        return response.results[0]['timestamp']
    return None


def check_partitions(database, table, hive_cursor):
    # Recupero delle partizioni
    query_partitions = f"SHOW PARTITIONS {database}.{table}"
    hive_cursor.execute(query_partitions)
    partitions = hive_cursor.fetchall()

    if not partitions:
        print(f"La tabella {database}.{table} non ha partizioni.")
    else:
        print(f"Partizioni della tabella {database}.{table}:")
        for partition in partitions:
            print(partition[0])

            # Recupero delle sottopartizioni
            query_subpartitions = f"SHOW PARTITIONS {database}.{table} PARTITION ({partition[0]})"
            hive_cursor.execute(query_subpartitions)
            subpartitions = hive_cursor.fetchall()

            if subpartitions:
                print(f"\tSottopartizioni:")
                for subpartition in subpartitions:
                    print(f"\t{subpartition[0]}")


def analyze_partitions(database, table, hive_cursor):
    # Recupero delle partizioni
    query_partitions = f"SHOW PARTITIONS {database}.{table}"
    hive_cursor.execute(query_partitions)
    partitions = hive_cursor.fetchall()

    if not partitions:
        print(f"La tabella {database}.{table} non ha partizioni.")
    else:
        print(f"Analisi delle partizioni della tabella {database}.{table}:")
        for partition in partitions:
            print(f"\nPartizione: {partition[0]}")

            # Analisi statistica dettagliata per colonne
            query_analyze = f"DESCRIBE EXTENDED {database}.{table} PARTITION ({partition[0]})"
            hive_cursor.execute(query_analyze)
            analysis = hive_cursor.fetchall()

            for row in analysis:
                print("\t", row[0], ":", row[1])

            # Recupero delle sottopartizioni
            query_subpartitions = f"SHOW PARTITIONS {database}.{table} PARTITION ({partition[0]})"
            hive_cursor.execute(query_subpartitions)
            subpartitions = hive_cursor.fetchall()

            if subpartitions:
                print("\n\tSottopartizioni:")
                for subpartition in subpartitions:
                    print(f"\t{subpartition[0]}")
                    # Analisi statistica dettagliata per colonne
                    query_analyze_subpartition = f"DESCRIBE EXTENDED {database}.{table} PARTITION ({partition[0]},{subpartition[0]})"
                    hive_cursor.execute(query_analyze_subpartition)
                    analysis_sub = hive_cursor.fetchall()

                    for row in analysis_sub:
                        print("\t\t", row[0], ":", row[1])


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Connessione a Hive
    hive_conn = hive.Connection(host='0.0.0.0', port=10000, username='hive')
    hive_cursor = hive_conn.cursor()

    # Connessione a MongoDB
    mongo_client = MongoClient(host='0.0.0.0', port=27017)
    mongo_db = mongo_client['hive_queries']
    mongo_collection = mongo_db['queries']

    # Eseguiamo una query di analisi su Hive
    table_name = "ac.prt_ac_report"
    partition = "annomese_tipo_prt='202308_AC2'"
    query = execute_hive_analyze_query(table_name, partition)

    # Memorizziamo la query e il timestamp in MongoDB
    timestamp = datetime.now()
    save_query(query, timestamp)

    # Recuperiamo il timestamp dell'ultima query memorizzata
    last_query_timestamp = get_last_query_timestamp()
    print("Timestamp dell'ultima query di analisi: ", last_query_timestamp)

    # Esempio di utilizzo check_partitions
    database = "ac"
    table = "prt_ac_report"
    check_partitions(database, table, hive_cursor)

    # Esempio di utilizzo analyze_partitions
    analyze_partitions(database, table, hive_cursor)
