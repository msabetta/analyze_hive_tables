import csv
import random
from faker import Faker
from datetime import datetime

fake = Faker()

# Apriamo un nuovo file CSV in modalità scrittura
with open('impiegati.csv', 'w', newline='') as csvfile:
    # Definiamo i nomi delle colonne
    fieldnames = ['id', 'nome', 'cognome', 'dipartimento', 'stipendio', 'data_assunzione']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    # Scriviamo l'intestazione
    writer.writeheader()

    # Generiamo 500 record casuali
    for i in range(500):
        # Generiamo dati casuali per ogni campo
        id = i + 1
        nome = fake.first_name()
        cognome = fake.last_name()
        dipartimento = fake.random_element(elements=('Vendite', 'Marketing', 'Produzione', 'Amministrazione'))
        stipendio = round(random.uniform(20000, 80000), 2)
        data_assunzione = fake.date_between(start_date='-5y', end_date='today').strftime('%Y-%m-%d')

        # Scriviamo il record nel file CSV
        writer.writerow({'id': id,
                         'nome': nome,
                         'cognome': cognome,
                         'dipartimento': dipartimento,
                         'stipendio': stipendio,
                         'data_assunzione': data_assunzione})
